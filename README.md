# electrode

A simple and light framework to create powerful webapps, that can also be installed in desktop and smartphones.

Check out the [documentation website](https://squeak.eauchat.org/libs/electrode/).
Don't want to read and prefer just [test an electrode app](https://framagit.org/squeak/electrode-template).
