const _ = require("underscore");
const $$ = require("squeak/node");
const $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

const defaultConfig = {

  //
  //                              GENERAL

  /**
    DESCRIPTION: the name of your app
    TYPE: <string>
  */
  name: "this will be filled from your package.json file",

  /**
    DESCRIPTION: the version of your app
    TYPE: <string>
    NOTE: if you customize thie, be aware that it is used by service worker to cache your app, update it...
  */
  version: "this will be filled from your package.json file",

  /**
    DESCRIPTION: color for server start logging in terminal (the nice little "drawing" you'll see when you start the nodejs server ;))
    TYPE: <hexOrChalkColorString|hexOrChalkColorString[]|"rainbow">
    TYPES:
      hexOrChalkColorString = <
        | any hex color (e.g. #0FF0DE)
        | key of $$.log.node.chalk.colors
        | key of $$.log.node.chalk.bgColors
      >
  */
  logColor: "rainbow",

  /**
    DESCRIPTION: the port at which to serve your app (for nodejs server)
    TYPE: <integer>
  */
  port: 5000,

  /**
    DESCRIPTION: folder where your app is on your system
    TYPE: <string> «
      should be an absolute path
      the best way to set this, is to use the "path" module, require it with `const path = require("path")`
      then for example, if your config file is in a first level subfolder of your appPath will be: `path.join(__dirname +"/../")`
    »
  */
  appPath: "error this must be specified",

  /**
    DESCRIPTION:
      if set to true, will serve minified script and styles
      if set to false, will rebuild scripts and styles at every page call and serve them non minified
      (see wiki/modes page for details)
    TYPE: <boolean>
  */
  production: false,

  /**
    DESCRIPTION: if true, will display more debugging logs
    TYPE: <boolean>
  */
  debug: false,

  /**
    DESCRIPTION: url of the default favicon
    TYPE: <string>
  */
  faviconUrl: "/share/favicon.png",

  /**
    DESCRIPTION: url of the error page
    TYPE: <string>
  */
  errorUrl: "/error/",

  /**
    DESCRIPTION: Headers to set for all pages (this is customizable also on a per-page basis, from the routes pages list)
    TYPE: <{ [header-name]: <header-value> }> « be careful to only use valid header names and values in this object »
  */
  pageHeaders: {
    _recursiveOption: true,
    // set a default Content-Security-Policy value (see https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP for more info on the Content-Security-Policy header)
    // this CSP shouldn't allow execution of external scripts and styles, but is still fairly laxist for other types of contents
    "Content-Security-Policy": "default-src 'self' 'unsafe-inline'; media-src *; img-src * data: blob:; object-src * data: blob:; frame-src *; script-src 'self' 'unsafe-inline' 'unsafe-eval'; connect-src * data: blob:",
  },

  /**
    DESCRIPTION: setup caching of the app for offline usage (this setup allows the user to use the app even when they are offline, install them as Progressive Web App)
    TYPE: <{
      cache: <boolean> « setting this to true will enable caching with serviceWorker »,
      defaults: <false|{see below}> « whether electrode default resources should be cached (default favicon path, default fonts and icomoon) »,
      pages: <string[]> « a list of url of pages to cache »,
      customUrls: <string[]> « a list of custom urls to cache »,
      manifest: <false|{pwaManifest see link below for details}> «
        you can leave it to it's default value, or customize it to customize the manifest file of your pwa
        setting this to false will disable the possibility for the user to install your app as pwa (it will still allow them to use it offline if pwa.cache is true)
        for infos on PWAs see: https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Installable_PWAs
        for infos on PWAs manifest see https://developer.mozilla.org/en-US/docs/Web/Manifest
        NOTE: if cache is not enabled, this is likely not to work in some browsers (see the previous 'Progressive_web_apps' mozilla article)
      »,
    }>
  */
  pwa: {
    _recursiveOption: true,
    cache: false,
    defaults: {
      _recursiveOption: true,
      favicon: true,
      icomoon: true,
      font_lato: true,
      font_dejaVuSansMono: true,
      font_zillaSlab: false,
    },
    pages: [],
    customUrls: [],
    manifest: {
      _recursiveOption: true,
      name:        "this will be replaced below by your package.json 'name' value",
      short_name:  "this will be replaced below by your package.json 'name' value",
      description: "this will be replaced below by your package.json 'description' value",
      icons: [
        {
          "src": "/share/favicon.png",
          "sizes": "357x357",
          "type": "image/png"
        },
      ],
      start_url: "/",
      display: "standalone", // "fullscreen, ""standalone", "minimal-ui", or "browser"
      theme_color: $$.random.color(1),
      background_color: $$.random.color(1),
    },
  },

  /**
    DESCRIPTION:
      if false will build service worker script once,
      if true will build it at every request, this is convenient when you're developping and debugging caching of the app
    TYPE: <boolean>
  */
  refreshServiceWorkerScriptAtEachRequest: false,

  /**
    DESCRIPTION:
      to use authentication, provide a database url
      authentication is designed to work with couchdb, giving that you provide here the address of the "_users" database, electrode will verify that the user is a proper user of that couchdb instance, and password is right
      should be ok to be any database, but for now the id of the entries must be in the format: "org.couchdb.user:"+ username
      if database is not accessible publicly, you must set the proper username and password in the url
      also, the algorithm used to verify password is not customizable at the moment, so passwords must be stored hashed with the strategy used by couchdb in it's _users database (see electrode/router/authentication/users.js for details)
    TYPE: <{
      couchUrl: <string|null> « url of the database containing list of users »,
      ?loginUrl: <string> « url you should POST to when you want to login »,
      ?logoutUrl: <string> « url you should send GET request to when you want to logout »,
      ?logoutRedirect: <string> « the page that should be opened when logging out »,
      ?keysToLeaveInUserProfile: <string[]> « array of keys that shouldn't be removed from user's profile (by default: "_id", "_rev", "type", "password_scheme", "iterations", "derived_key", "salt" are removed) »,
      ?leavePasswordInUserProfile: <boolean> « if true, will send user's password with it's profile in plain text, under the key "p", it's not a great idea to use this, but if you really need to !? »
    }>
  */
  authentication: {
    _recursiveOption: true, // for internal use
    couchUrl: null,
    loginUrl: "/login/",
    logoutUrl: "/logout/",
    logoutRedirect: "/",
    keysToLeaveInUserProfile: null,
    leavePasswordInUserProfile: false,
  },

  /**
    DESCRIPTION:
      when importing less files, list all submodules recursively from "node_modules" directories
      if this is not false or null, it will allow submodules's node_modules directory to be searched for resolving less @import statements
      this is especially useful when developping in a local environment where some libraries having styles are served locally
    TYPE: <false|"always"|"once"> «
      if "always" will rebuild list on each page request,
      if "once", only once when first page is called and then will reuse the same list
    »
  */
  resolveSubmodulesPathsRecursivelyForLessImportStatments: false,

  //
  //                              PATHS

  /**
    DESCRIPTION: how some folders paths are translated as url in the browser
    TYPE: <{
      pagesData: <string> « root url where to serve pages contents »,
    }>
  */
  browserPath: {
    pagesData: "/pages/",
  },

  /**
    DESCRIPTION:
      paths to directories containing elements necessary to electrode to make app
      this defines the folders structure in the app you are creating
    TYPE: <{
      [key]: <string> « path relative to app root »
    }>
    NOTE: a "paths" key will be generated with all the following relative paths made absolute paths
  */
  relativePath: {

    _recursiveOption: true, // for internal use

    // where scripts and styles assets are stored to be served to browser (see wiki/architecture#dis)
    dist: "dist/",
    // global files (see wiki/architecture#global)
    global: "global/",
    // pages scripts and styles (see wiki/architecture#pages)
    pages: "pages/",
    // public assets (see wiki/architecture#public)
    public: "public/",
    // pages public assets (see wiki/architecture#public)
    pagesPublic: "public/pages/",
    // routes and custom routes (see wiki/architecture#routes)
    routes: "routes/",
    customRoutes: "routes/custom/",

  },

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (preConfig) {

  if (!preConfig.appPath) throw "Cannot initialize electrode configuration if your boot/config.js file doesn't contain a path."

  else {

    var defaultConfigCloned = _.clone(defaultConfig);

    // FILL NAME AND VERSION FROM package.json
    try {
      let appPackageJSON = require(preConfig.appPath +"package.json");
      defaultConfigCloned.name = appPackageJSON.name;
      defaultConfigCloned.version = appPackageJSON.version;
      defaultConfigCloned.pwa.manifest.name = appPackageJSON.name;
      defaultConfigCloned.pwa.manifest.short_name = appPackageJSON.name;
      defaultConfigCloned.pwa.manifest.description = appPackageJSON.description;
      // useful for properly displaying free software license
      defaultConfigCloned.license = appPackageJSON.license;
      defaultConfigCloned.repository = appPackageJSON.repository;
    }
    catch (e) {
      $$log.error("Failed to get name and version from your app package.json file. — Is your app missing this file?");
    };

    // RETURN DEFAULT CONFIG WITH NAME AND VERSION FILLED
    return defaultConfigCloned;

  };

};
