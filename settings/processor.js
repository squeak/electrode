let fs = require("fs");
let path = require("path");
let _ = require("underscore");
let $$ = require("squeak/node");
let defaultConfig = require("./defaultConfig.js");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CONFIG PROCESSOR
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (preConfig) {

  //
  //                              GET CONFIG

  let appConfig = $$.defaults(defaultConfig(preConfig), preConfig);

  //
  //                              MAKE ABSOLUTE PATHS

  appConfig.path = _.mapObject(appConfig.relativePath, function (path, key) {
    if (key === "_recursiveOption") return path
    else return appConfig.appPath + path;
  });

  //
  //                              RETURN FULL CONFIG

  return appConfig

  //                              ¬
  //

};
