var _ = require("underscore");
var $$ = require("squeak/node");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  CUTE RAINBOW LINE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeRandomLine (length) {

  var possibleCharacters = "^|.;:/?,·°)(&\")'§!-_–*$`£%+=<>¬♦   OOOO        X   ";

  var lineToLog = "";
  for (i=0; i<length; i++) {
    lineToLog += $$.random.entry(possibleCharacters);
  }

  return lineToLog;

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RAINBOW COLORED SHAPE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: logs a multicolor quadrilataire of the given dimension
  ARGUMENTS: (
    width <number>,
    heightIncrement <[number, number]> « min and max of the random increment »
  )
  RETURN: void
*/
function logRandomQuadrilataire (width, heightIncrement, color) {

  logRandomTriangle(width, heightIncrement, "up", color);
  logRandomTriangle(width, heightIncrement, "down", color);

};

/**
  DESCRIPTION: logs a colored triangle of the given dimension
  ARGUMENTS: (
    !width <number>,
    !heightIncrement <[number, number]> « min and max of the random increment »,
    !direction <"up"|"down"> « pass up to have the tip of the triangle on top, anything else (but preferably "down") for tip in the bottom »,
    ?color <see $$.log.node.style·color>,
  )
  RETURN: <void>
*/
function logRandomTriangle (width, heightIncrement, direction, color) {

  if (direction == "up") var test = function () { return lineToLog.length < width-increment }
  else var test = function () { return lineToLog.length > 0 };

  var lineToLog = makeRandomLine( direction == "up" ? 1 : width);
  var increment = $$.random.integer(heightIncrement);
  var whiteSpacePosition = $$.random.entry([ $$.random.number(1,2), $$.random.integer(2,width/2) ]);

  var occurence = 0;
  var lines = []

  while (test()) {

    occurence++;

    var whiteSpaces = "";
    for (i=0; i<(width-lineToLog.length)/whiteSpacePosition; i++) { whiteSpaces += " " };
    $$.log(whiteSpaces + $$.log.node.style(lineToLog, color));

    lineToLog = makeRandomLine(lineToLog.length + increment*( direction == "up" ? 1 : -1));

  }

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  EXPORT LISTENING
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (appConfig) {

  var triangleWidth = 89;

  // READY to LOG
  var readyString = "  "+ appConfig.name +" electrode server listening on port "+ appConfig.port +"  ";

  // BARS UP AND DOWN FROM IT
  // var barsString = _.map(readyString, function(){ return "|" }).join("");

  // SPACES TO CENTER IT
  var postAndSuffix = makeRandomLine((triangleWidth-readyString.length)/2);
  // var spacesString = "";
  // for (i=0; i<(triangleWidth-readyString.length)/2; i++) { spacesString += " " };

  // LOGS
  $$.log();
  logRandomTriangle(triangleWidth, [5, 9], "up", appConfig.logColor);
  $$.log($$.log.node.style(postAndSuffix, appConfig.logColor) + $$.log.node.style(readyString, appConfig.logColor, { bold: true, }) + $$.log.node.style(postAndSuffix, appConfig.logColor));
  logRandomTriangle(triangleWidth, [5, 9], "down", appConfig.logColor);
  $$.log();

}
