let _ = require("underscore");
let $$ = require("squeak/node");
let initializeAuthentication = require("../authentication/initialize");
let $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE LOGIN
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function serveLogin (app, authConfig) {
  app.post(
    // o            >            +            #            °            ·            ^            :            |

    // url of login page
    authConfig.loginUrl,

    // o            >            +            #            °            ·            ^            :            |

    // trigger passport authentication
    function (req, res, next) {

      //
      //                              LOG THE POST REQUEST

      $$log.custom("[POST] "+ req.originalUrl, { logColor: "green" });

      //
      //                              URL REDIRECTION MAKER

      function makeLoginPageUrl (status) {
        return $$.url.make({
          url: authConfig.loginUrl,
          queryObject: { status: status, url: req.query.url, },
        });
      };

      //
      //                              PASSPORT AUTHENTICATE SETUP

      authConfig.passport.authenticate("local-login", function (err, user, info) {

        // SYSTEM ERROR (maybe couch is not found or something similar)
        if (err) {
          $$log.error("System error trying to log a user in:", err);
          if (req.query.noRedirect) return res.status(500).send({ ok: false, status: "error", });
          else return res.redirect(makeLoginPageUrl("error"));
        }

        // USER FOUND, OK
        // so now the user will be logged in, but router/authentication/verify.js script will also check if the user is allowed to enter the page
        else if (user) req.logIn(user, function (loginError) {

          if (loginError) {
            $$log.error("Passport login error:", loginError);
            if (req.query.noRedirect) return res.status(500).send({ ok: false, status: "error", });
            else return res.redirect(makeLoginPageUrl("error"));
          }
          else {
            $$log.custom("✔ Successfully logged in '"+ user.name +"' user", { logColor: "yellowBright" });
            // redirect user to the page they were asking, or send ok response
            if (req.query.noRedirect) return res.status(200).send({ ok: true, message: "Successfully logged in.", });
            else return res.redirect(req.query.url || "/");
          };

        })

        // MISTAKE IN USERNAME OR PASSWORD?
        else if (info) {
          if (info.status == "autherror") $$log.error("Username or password error attempting to login as '"+ info.username +"'")
          else $$log.error("Unknown error trying to login '"+ info.username +"':", info);
          if (req.query.noRedirect) return res.status(401).send({ ok: false, status: info.status, });
          else return res.redirect(makeLoginPageUrl(info.status));
        }

        // OTHER UNEXPECTED ERROR
        else {
          $$log.error("Unknown error trying to log a user in");
          if (req.query.noRedirect) return res.status(500).send({ ok: false, status: "error", });
          else return res.redirect(makeLoginPageUrl("error"));
        };

      })(req, res, next);

      //                              ¬
      //

    }

    // o            >            +            #            °            ·            ^            :            |
  );
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE LOGOUT
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function serveLogout (app, authConfig) {
  app.get(authConfig.logoutUrl, function (req, res) {
    $$log.custom("✔ Successfully logged out '"+ (req.user ? req.user.name : "¿?") +"' user", { logColor: "yellowBright" })
    req.session.destroy(function (err) {
      res.redirect(authConfig.logoutRedirect);
    });
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (app, authConfig) {
  initializeAuthentication(authConfig);
  serveLogin(app, authConfig);
  serveLogout(app, authConfig);
};
