const _ = require("underscore");
const $$ = require("squeak/node");
const async = require("async");
const loadHtml = require("../loader");
const bundler = require("../bundler");
const authenticateVerify = require("../authentication/verify");
const $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE PAGES LIST
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makePagesList (appConfig) {

  var pages = require(appConfig.path.routes +"pages.js");

  _.each(pages, function(page, url){

    // FILL NAME, PATH AND TITLE WHEN THEY ARE NOT SPECIFIED
    if (!page.name) page.name = $$.match(page.url.replace(/\/*$/, ""), /[^\/]*$/);
    if (!page.title) page.title = page.name;
    if (!page.path) page.path = page.url.replace(/^\//, "");
    if (!page.data) page.data = appConfig.browserPath.pagesData + page.path;

    // GET DIRECT CHILDREN OF EVERY PAGE
    if (!page.children) page.children = _.filter(pages, function (onePage) {
      return $$.match(onePage.url, "\^"+ page.url +"\[\^\\/\]\+\\/\$");
    });

  });

  return pages

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE PAGE UTILITIES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: verify if the given user is allowed into this page
  ARGUMENTS: (
    pageAuth <{ users: <string[]>, roles: <string[]> }>
  )
  RETURN: <boolean>
*/
function authentifiedUserIsAllowed (pageAuth, authentifiedUser) {
  // no user authentified
  if (!authentifiedUser) return false
  // user authorized
  else if (_.indexOf(pageAuth.users, authentifiedUser.username) != -1) return true
  // role authorized
  else if (_.intersection(pageAuth.roles, authentifiedUser.roles).length) return true
  // unauthorized
  else return false;
}

/**
  DESCRIPTION:
    recurse children of a page to see if they should be removed from listing before sending response
    also removes detailed authentication informations from all page objects
  ARGUMENTS: (
    pageObject <{
      auth: <see authentifiedUserIsAllowed pageAuth>,
      children: pageObject[]
      ...
    }>,
    authenticatedUser <{
      username: <string>,
      roles: <string[]>,
      ...
    }>,
  )
  RETURN: void
*/
function removeChildrenPagesIfAuthenticationNotOk (pageObject, authenticatedUser) {

  // remove children pages for pages needing authentication if the user is not allowed
  if (pageObject.auth && !authentifiedUserIsAllowed(pageObject.auth, authenticatedUser)) pageObject.children = []
  else _.each(pageObject.children, function (childPageObject) {
    removeChildrenPagesIfAuthenticationNotOk(childPageObject, authenticatedUser);
  });

  // remove detailed informations about page authentication to the response, and keep it to the basic
  if (pageObject.auth) pageObject.auth = true;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REDIRECT TO LOGIN PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function redirectToLoginPage (req, res, authConfig, status) {
  var redirectionUrl = $$.url.make({
    url: authConfig.loginUrl,
    queryObject: { status: status, url: req.url, },
  });
  return res.redirect(redirectionUrl);
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create get route to the given page
  ARGUMENTS: (
    app <express app>,
    appConfig,
    page,
    done <function(ø)>
  )
  RETURN: void
*/
function servePage (app, appConfig, page, done) {

  app.get(

    // page url
    page.url,

    // authenticate check
    _.partial(authenticateVerify, appConfig.authentication, page.auth, redirectToLoginPage),

    // if authenticated ok: serve
    function (req, res) {

      // clone page not to modify the original object
      var pageCloneToSend = $$.clone(page, 99);

      // log
      $$log.custom("[GET] "+ req.originalUrl, { logColor: "green" });

      // if authentication is enabled
      if (appConfig.authentication && appConfig.authentication.couchUrl) {

        // get currently authenticated user
        if (req.isAuthenticated()) {
          pageCloneToSend.authenticatedUser = _.clone(req.user);
          // remove details set by couchdb
          var keysToRemove = ["_id", "_rev", "type", "password_scheme", "iterations", "derived_key", "salt"];
          _.each(_.difference(keysToRemove, appConfig.authentication.keysToLeaveInUserProfile), function (key) {
            delete pageCloneToSend.authenticatedUser[key];
          });
        }
        else pageCloneToSend.authenticatedUser = null;

        // check which page should not be listed when sent because the user is not properly authenticated
        removeChildrenPagesIfAuthenticationNotOk(pageCloneToSend, pageCloneToSend.authenticatedUser);

      };

      // LOAD HTML
      loadHtml(appConfig, pageCloneToSend, function (htmlContent) {

        // set response content type
        res.set("Content-Type", "text/html");

        // set response headers
        let pageHeaders = $$.defaults(appConfig.pageHeaders, page.pageHeaders);
        delete pageHeaders._recursiveOption;
        _.each(pageHeaders, function (headerValue, headerTitle) {
          res.setHeader(headerTitle, headerValue);
        });

        // send response
        res.send(Buffer.from(htmlContent));

      });

    }

  );

  // build script and style bundle for page (only in production mode, otherwise it's built at every request)
  if (appConfig.production) bundler(appConfig, page, done)
  else done();

}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REDIRECT PAGE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function redirectPage (app, page, done) {
  app.get(page.url, function(req, res, next) {
    // log
    $$log.custom("[GET] Redirected "+ req.originalUrl +" to "+ page.redirect, { logColor: "yellow" });
    // redirect
    res.redirect(page.redirect);
  });
  done();
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (app, appConfig, callback) {

  app.pagesConfigsList = makePagesList(appConfig);

  // LOG THAT BUNDLES ARE BEING BUILT IF PRODUCTION (SO USER KNOW EVERYTHING IS GOING RIGHT)
  if (appConfig.production) $$log.custom("Started building "+ _.size(app.pagesConfigsList) +" page bundles... This could take a while...", { logColor: "yellow" });

  // MAKE PAGE ROUTES AND BUNDLES
  async.eachSeries(app.pagesConfigsList, function (page, asyncIterationIncrement) {

    // SERVE PAGE
    if (!page.redirect) servePage(app, appConfig, page, asyncIterationIncrement)

    // REDIRECT PAGE
    else redirectPage(app, page, asyncIterationIncrement);

  }, callback);


};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
