let _ = require("underscore");
let $$ = require("squeak/node");
let demeter = require("demeter");
let authenticateVerify = require("../authentication/verify");
let $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function makeCustomRoutesList (customRoutesPath) {

  // list all files in custom routes folder
  return _.chain(demeter.lsSync({
    browserPath: customRoutesPath,
    serverPrefixPath: "",
    recursive: true,
    flat: true,
  })).map(function(file){

      // get script's content
      var customRoutes = require(file.path);

      // get path of the file relative to custom routes folder
      var relativePath = "/"+ file.path.replace(customRoutesPath, "").replace(/\.js$/, "/");

      return _.map(customRoutes, function(customRoute){

        // if path is relative prepend file's path
        if (!customRoute.url.match(/^\//)) customRoute.url = relativePath + customRoute.url;

        return customRoute;

      });

    })
    .flatten()
    .value()
  ;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  RESPOND ACCESS WAS DENIED
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function respondAccessDenied (req, res, authConfig, status) {
  return res.status(status == "unauthorized" ? 403 : 401).send({
    ok: false,
    url: req.url,
    status: status,
    message: "You have been denied access to this ressource. Response status: '"+ status +"'.",
  });
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (app, appConfig) {

  var customRoutes = makeCustomRoutesList(appConfig.path.customRoutes);

  _.each(customRoutes, function (customRoute) {
    app[customRoute.type.toLowerCase()](

      // url
      customRoute.url,

      // authenticate check
      _.partial(authenticateVerify, appConfig.authentication, customRoute.auth, respondAccessDenied),

      // if authenticated ok: serve
      function (req, res, next) {
        // by default, custom requests responses shouldn't be cached, this specifies it (leaving free to modify this default individually from the custom request code)
        res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        // log
        $$log.custom("["+ customRoute.type +"] "+ req.originalUrl, { logColor: "green" });
        // route
        customRoute.func.apply(this, arguments);
      }

    );
  });

}
