const _ = require("underscore");
const $$ = require("squeak/node");
const loadHtml = require("../loader");
const $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE ERROR PAGE FUNCTION
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function sendErrorPage (app, appConfig, req, res, errorCode) {

  // get error page config
  var errorPageConfig = _.findWhere(app.pagesConfigsList, { url: appConfig.errorUrl, });

  // no error page has been set
  if (!errorPageConfig) return res.status(errorCode).send("Internal server error.");

  // server set error page
  else loadHtml(appConfig, errorPageConfig, function (htmlContent) {
    res.set("Content-Type", "text/html");
    res.status(errorCode);
    res.send(Buffer.from(htmlContent));
  });

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (app, appConfig) {

  // handle request not found 404
  app.use(function(req, res, next) {
    $$log.warning("Error serving '"+ req.url +"', probably it's not a valid path, error page was served instead");
    sendErrorPage(app, appConfig, req, res, 404);
  });

  // handle internal server error 500
  app.use(function(err, req, res, next) {
    $$log.error("Internal server error!", "Request url was: '"+ req.url +"'", err);
    sendErrorPage(app, appConfig, req, res, 500);
  });

};
