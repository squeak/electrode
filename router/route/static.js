let express = require("express");
let path = require("path");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (app, appConfig) {

  // SERVE PUBLIC FILES // NOTE: after routes, otherwise seem to capture them
  app.use('/', express.static(appConfig.path.public));

  // SERVE PUBLIC FILES // NOTE: after routes, otherwise seem to capture them
  app.use('/dist/', express.static(appConfig.path.dist));

  // SERVE ELECTRODE GLOBAL FILES (necessary for some ui or fonts elements)
  app.use('/electrode/', express.static(path.join(__dirname +"/../../global/")));

  // SERVE STYLECTRODE GLOBAL FILES (necessary for some ui or fonts elements)
  app.use('/stylectrode/', express.static(path.join(__dirname +"/../../node_modules/stylectrode/"))); // when electrode is installed locally
  app.use('/stylectrode/', express.static(path.join(__dirname +"/../../../stylectrode/"))); // when electrode is installed from npm

};
