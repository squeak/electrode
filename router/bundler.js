const fs = require('fs');
const path = require("path");
const less = require("less");
const browserify = require("browserify");
const uglifyify = require("uglifyify");
const _ = require("underscore");
const $$ = require("squeak/node");
require("squeak/extension/array");
const $$log = $$.logger("electrode");
const demeter = require("demeter");
// resolve path to electrode/global/
var electrodeDirectory = path.join(__dirname, "../");
var electrodeGlobalDirectory = electrodeDirectory +"global/";

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  GET LIST OF ALL CHILDREN node_modules PACKAGES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let savedSubModulesPaths;
function addListOfSubmodulesPaths (importResolvePaths, appConfig) {
  let subModulesPaths;
  if (savedSubModulesPaths) subModulesPaths = savedSubModulesPaths
  else {
    subModulesPaths = getListOfSubModulesPaths(appConfig.appPath +"node_modules/", 100);
    $$log.success("Generated custom list of paths to resolve less @import statements");
    // save list globally if should only get list once
    if (appConfig.resolveSubmodulesPathsRecursivelyForLessImportStatments == "once") savedSubModulesPaths = subModulesPaths;
  };
  return $$.array.merge(importResolvePaths, subModulesPaths);
};

/**
  DESCRIPTION: recursively search for submodules paths (search if packages in node_modules have also node_modules inside them)
  ARGUMENTS: (
    !pathsToSearch <string>,
    ?maxDepth <integer> « how deep to search at maximum (may be useful in case of infinite recursion because of nested symlinks) »,
  )
  RETURN: <string[]>
*/
function getListOfSubModulesPaths (pathsToSearch, maxDepth) {
  let pathsList = [];

  // iterate list of children directories, and find submodules recursively
  _.each(
    demeter.lsSync({ browserPath: pathsToSearch, }),
    function (fileObject) {
      if (
        fileObject.isDirectory &&
        fileObject.name != "~" &&
        fs.existsSync(fileObject.path +"node_modules/")
      ) {
        pathsList.push(fileObject.path +"node_modules/");
        if (maxDepth !== 0) pathsList.push(getListOfSubModulesPaths(fileObject.path +"node_modules/", maxDepth -1));
      };
    }
  );

  return _.chain(pathsList).flatten().compact().value();
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BUNDLE STYLES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function bundleCSS (appConfig, pageStyles, pageBundlesPrefix, callback) {

  //
  //                              GET LIST OF PATHS OF FILES TO BUNDLE

  // bunle electrode.less + page styles + app themes at last
  var allStyles = [
    // electrode and app styles
    fs.readFileSync(electrodeGlobalDirectory +"electrode.less").toString(),
    // page styles
    fs.readFileSync(pageStyles).toString(),
  ];
  var lessFilesToBundle =  allStyles.join("\n");

  //
  //                              MAKE LIST OF POSSIBLE PATHS TO RESOLVE @import STATEMENTS IN FILES

  let importResolvePaths = [
     // electrode/global/
     electrodeGlobalDirectory,
     // global/
     appConfig.path.global,
     // node_modules/
     appConfig.appPath +"node_modules/",
     // public/lib/
     appConfig.path.public +"lib/",
     // electrode/lib/
     electrodeDirectory +"lib/",
   ];
   if (appConfig.resolveSubmodulesPathsRecursivelyForLessImportStatments) importResolvePaths = addListOfSubmodulesPaths(importResolvePaths, appConfig);

  //
  //                              RENDER LESS FILES TO CSS

  less.render(lessFilesToBundle, {
    // specify path of the called less file so imports work
    filename: path.resolve(pageStyles),
    // alternative places to find matching files to import
    paths: importResolvePaths,
  }, function (e, css) {

    // LESS COMPILE ERROR
    if (e) {

      // error with style file path
      if (_.isString(e.message) && e.message.match(". Tried - ")) {
        let messageParts = e.message.split(". Tried - ");
        delete e.message;
        $$.log.detailedError(
          "electrode/router/bundler",
          "Failed to load styles for '"+ e.filename +"'.\n\n"+ messageParts[0] +", the following paths where tried:",
          $$.json.pretty(messageParts[1].split(",")),
          $$.json.pretty(e)
        );
      }

      // error in file or else
      else {
        $$.log.detailedError(
          "electrode/router/bundler",
          "Failed to load styles for '"+ e.filename +"'.",
          $$.json.pretty(e)
        );
      };

    }

    // WRITE CSS BUNDLE TO FILE
    else if (appConfig.production) fs.writeFileSync(pageBundlesPrefix +".min.css", css.css) // TODO: make real minified version for production (when implementing this, make sure to implement as well the possibility to omit/force minification with page.minify option)
    else fs.writeFileSync(pageBundlesPrefix +".css", css.css);

    // CALLBACK WHEN DONE
    callback();

  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  BUNDLES SCRIPTS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function bundleJS (appConfig, pageScript, pageBundlesPrefix, page, callback) {

  //
  //                              CREATE BUNDLER

  let bundler = browserify();
  // add electrode global js
  bundler.add(electrodeGlobalDirectory +"electrode.js");
  // add app global js
  bundler.add(appConfig.path.global +"app.js");
  // add page script
  bundler.add(pageScript);

  //
  //                              BUILD BUNDLES

  let bundleFileStream;

  // figure out file path depending on mode
  let filePath = appConfig.production ? pageBundlesPrefix +".min.js" : pageBundlesPrefix +".js";

  // create bundle file stream
  bundleFileStream = fs.createWriteStream(filePath);

  // create bundle object
  let toBundle = bundler;

  // transform bundle if minify is true, or if it's undefined and production mode is on
  if (_.isUndefined(page.minify) ? appConfig.production : page.minify) toBundle = toBundle.transform(uglifyify, { global: true  });
  // TODO: since bundling can be ignored on some pages, if they have already been done, page.minify option may not be properly taken into consideration
  // This is an edge case, where a same bundle would ask for minified or non minified, it could be nice to address it some day.

  // make bundle and write it to file
  toBundle
    .bundle()
    .pipe(bundleFileStream)
  ;

  // callback when done
  bundleFileStream.on("finish", callback);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

let productionCreatedBundlesCache = [];

module.exports = function (appConfig, page, callback) {

  //
  //                              MAKE SURE THAT A PAGE PATH DOESN'T START WITH ../
  // (this would create bad things, putting dist files somewhere else than the dist folder)

  if (page.path.match(/^\.\.\//)) {
    throw [
      "You tried to use a page path starting with ../",
      "Electrode cannot support such syntax, dist files would end up in the wrong directory.",
      "If you need to serve pages that have scripts in another directory, either change the value of 'path.pages' in your config, or set a 'customPagesFolderPath' for this page in your routes.",
    ];
  };

  //
  //                              PAGE'S SCRIPT AND STYLES LOCATIONS

  // TODO: using customPagesFolderPath, different bundles could be written to the same output! find a fix
  var pagesPath = page.customPagesFolderPath ? appConfig.appPath + page.customPagesFolderPath : appConfig.path.pages;
  var pageScript = pagesPath + page.path + page.name +".js";
  var pageStyles = pagesPath + page.path + page.name +".less";
  var pageBundlesFolder = appConfig.path.dist + page.path;
  var pageBundlesPrefix = pageBundlesFolder + page.name;

  //
  //                              IF IN PRODUCTION AND THIS BUNDLE HAS ALREADY BEEN GENERATED, JUST DO NOTHING
  if (appConfig.production && _.indexOf(productionCreatedBundlesCache, pageBundlesPrefix) != -1) {
    $$log.success("Omited rebundling scripts and styles for: "+ page.url);
    callback();
    return;
  };

  //
  //                              MAKE SURE BUNDLE DESTINATION DIRECTORY EXISTS

  !fs.existsSync(pageBundlesFolder) && fs.mkdirSync(pageBundlesFolder, { recursive: true });

  //
  //                              BUNDLES MAKERS

  function makeScripts (next) {
    if (!fs.existsSync(pageScript)) next(" ✖ Missing page script for page "+ page.url)
    else bundleJS(appConfig, pageScript, pageBundlesPrefix, page, next);
  };

  function makeStyles (next) {
    if (!fs.existsSync(pageStyles)) next(" ✖ Missing page styles for page "+ page.url)
    else bundleCSS(appConfig, pageStyles, pageBundlesPrefix, next);
  };

  //
  //                              MAKE BUNDLES

  var thereWasSomeError = false;
  function alertError (err, scriptOrStyle) {
    if (err) {
      $$log.error("Error bundling "+ scriptOrStyle, err);
      thereWasSomeError = true;
    };
  };

  makeScripts(function (errScripts) {
    alertError(errScripts, "script");
    makeStyles(function (errStyles) {
      alertError(errStyles, "style");
      if (appConfig.production) productionCreatedBundlesCache.push(pageBundlesPrefix); // save that this bundle has already been generated
      $$log.custom("Finished bundling scripts and styles for: "+ page.url, thereWasSomeError ? { logType: "warning" } : { logType: "success" });
      callback();
    });
  });

  //                              ¬
  //

};
