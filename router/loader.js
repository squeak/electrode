let fs = require("fs");
let path = require("path");
let _ = require("underscore");
let $$ = require("squeak/node");
let bundler = require("./bundler");
let utilities = require("./utilities");

//
//                              GET index.html TEXT

var indexHtmlContent = fs.readFileSync(path.join(__dirname, "index.html"));

//
//                              MAKE CUSTOM HTML STRING FOR THE GIVEN VARIABLES

var loadIndexHtml = ({title, pageStyles, pageScript, variables, faviconUrl, licenseNotice}) => {
  return eval('`'+ indexHtmlContent +'`')
}

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE RESPONSE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = (appConfig, page, callback) => {

  // get package json content
  var packageJson = require(appConfig.appPath +"package.json");

  // re-bundle on every request only when not on production
  if (appConfig.production) next()
  else bundler(appConfig, page, next);


  function next () {

    // MAKE LIST OF VARIABLES TO EMBED IN ALL PAGES
    var electrodeVariable = {
      appName: appConfig.name || packageJson.name,
      appVersion: packageJson.version,
      mode: appConfig.production ? "prod" : "dev",
      pwaCache: appConfig.pwa && appConfig.pwa.cache ? true : false,
      pwaManifest: appConfig.pwa && appConfig.pwa.manifest ? true : false,
    };

    // MAKE LICENSE TEXT
    let licenseNotice = [];
    if (appConfig.license) licenseNotice.push(
      "// The code of this page (html, scripts, styles... except for the used libraries) is licensed as follow:",
      "// @license: "+ appConfig.license,
      "// Used libraries are all free software, listed in the repository/source. You can find their license in their own repositories."
    );
    if (appConfig.repository) licenseNotice.push(
      "// The source code for this page is available at the following address:",
      "// @source: "+ (_.isObject(appConfig.repository) ? appConfig.repository.url : appConfig.repository)
    );

    // CREATE HTML STRING FOR CALLBACK
    var html = loadIndexHtml({
      title: page.title,
      pageStyles: utilities.makePageDistPath(appConfig, page, "css"),
      pageScript: utilities.makePageDistPath(appConfig, page, "js"),
      faviconUrl: appConfig.faviconUrl,
      variables: "var page = "+ JSON.stringify(page) +";\nvar electrode = "+ JSON.stringify(electrodeVariable) +";",
      licenseNotice: licenseNotice.join("\n"),
    });

    callback(html);

  };

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
