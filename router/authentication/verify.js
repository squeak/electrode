let _ = require("underscore");
let $$ = require("squeak/node");
require("squeak/extension/url");
let $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: verify if user is defined, if it is allowed to access this page, and respond in consequence
  ARGUMENTS: (
    !authConfig,
    !authObject,
    !failedLoggingCallback <function(req, res, authConfig, status <string>)>,
    !req,
    !res,
    !next,
  )
  RETURN: <?>
*/
module.exports = function (authConfig, authObject, failedLoggingCallback, req, res, next) {

  //
  //                              AUTH VERIFICATION
  // TODO: it would be nice to have the appropriate error code on redirections, so that redirecting to login page shows not as 200 (but maybs it's fine like this as well, see also: https://webmasters.stackexchange.com/questions/24443/should-i-return-a-http-401-status-code-on-an-html-based-login-form)

  // no auth needed
  if (!authObject || !authConfig || !authConfig.couchUrl) return next()

  // auth needed
  else {
    // logged in
    if (req.isAuthenticated()) {
      // user authorized
      if (_.indexOf(authObject.users, req.user.name) != -1) return next()
      // role authorized
      else if (_.intersection(authObject.roles, req.user.roles).length) return next()
      // user unauthorized
      else {
        $$log.error("Prevented '"+ req.user.name +"' user from accessing '"+ req.url +"'");
        return failedLoggingCallback(req, res, authConfig, "unauthorized");
      };
    }
    // not logged in
    else return failedLoggingCallback(req, res, authConfig, "mustlog");
  };

  //                              ¬
  //

};
