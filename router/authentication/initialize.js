let _ = require("underscore");
let $$ = require("squeak/node");
let $$log = $$.logger("electrode");

let users = require("./users.js");
let LocalStrategy   = require("passport-local").Strategy;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (authConfig) {

  //
  //                              PASSPORT SESSION SETUP
  // required for persistent login sessions
  // passport needs ability to serialize and unserialize users out of session

  // used to serialize the user for the session
  authConfig.passport.serializeUser(function(user, done) {
    done(null, user._id);
  });
  // used to deserialize the user
  authConfig.passport.deserializeUser(users.getById);

  //
  //                              LOCAL LOGIN
  // let's use named strategies in case we want to have more than one
  // by default, if there was no name, it would just be called 'local'

  authConfig.passport.use(
    // STRATEGY NAME
    "local-login",
    // STRATEGY
    new LocalStrategy(
      // o            >            +            #            °            ·            ^            :            |

      // OPTIONS
      // {
      //   // by default, local strategy uses username and password, if necessary, change these values
      //   usernameField : "username",
      //   passwordField : "password",
      //   // passReqToCallback : true // allows us to pass back the entire request to the callback
      // },

      // CALLBACK WITH USERNAME AND PASSWORD FROM THE LOGIN FORM
      // function (req, username, password, done) {
      function (username, password, done) {

        users.getByUsername(authConfig, username, password).then(function (response) {

          // ERROR
          if (response.error) {
            // user is missing, or password is incorrect
            if (response.error == "not_found" || response.error == "unauthorized") done(null, false, { username: username, status: "autherror" })
            // other error ???
            else done(null, false, { username: username, status: "error", couchResponse: response, });
          }
          // NO ERROR, PASS USER TO RESPONSE
          else {
            if (authConfig.leavePasswordInUserProfile) response.p = password // FIXME: find another way to achieve what this is doing!
            done(null, response);
          };

        }).catch(function (err) {
          $$log.error("Failed to get user by username: ", username);
          done(err);
        });

      }
      // o            >            +            #            °            ·            ^            :            |
    )
  );

  //                              ¬
  //

};
