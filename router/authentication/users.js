const _ = require("underscore");
const $$ = require("squeak/node");
const fetch = require("node-fetch");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE FULL USER PROFILE URL
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make full url to request user profile with username, password, couchdb url, _users db name, user profile entry id
  ARGUMENTS: (
    !couchUrl <url> « https://domain.tld (final slash doesn't matter, put it or not, as you like) »
    !username <string>,
    !password <string>,
  )
  RETURN: <string> «
    the full url to request the user's profile on couch, something like this:
    "https://username:password@couch.domain.tld/_users/org.couch.user:username"
  »
*/
function makeUrlToRequestUserProfile (couchUrl, username, password) {
  var protocolMatcher = /^https?:\/\//;
  var couchProtocol = $$.match(couchUrl, protocolMatcher);
  var couchUrlWithoutProtocol = $$.replace(couchUrl, protocolMatcher, "").replace(/\/*$/, "");
  return couchProtocol + username +":"+ $$.url.urlify(password) +"@"+ couchUrlWithoutProtocol +"/_users/org.couchdb.user:"+ username;
};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  LAST CONNECTED USERS CACHE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// list of users that have been connected recently
var usersCache = {};

// every half hour, cleanup users who were last requested since more than an hour
setInterval(function () {
  var now = new Date().getTime();
  _.each(usersCache, function (cachedUser, userId) {
    if (now - cachedUser.time > 1000 * 3600) delete usersCache[userId];
  });
}, 1000 * 1800);

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //                                                  GET USER
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  /**
    DESCRIPTION: get user from list of cached users
    ARGUMENTS: (
      !id <string>,
      !callback <function(err, ?user),
    )
    RETURN: <void>
  */
  getById: function (id, callback) {
    if (!usersCache[id]) callback({
      error: "electrodes user not found",
      searchedUser: id,
      usersCache: usersCache,
    })
    else {
      // update user's time
      usersCache[id].time = new Date ().getTime();
      // send user
      callback(null, usersCache[id].user);
    };
  },

  /**
    DESCRIPTION: make request for user to couch and return what it answers, also will cache success results to list of users that just visited
    ARGUMENTS: (
      !authConfig <{ couchUrl: <string>, },
      !username,
      !password,
    )
    RETURN: <Promise:<couchResponseObject>>
  */
  getByUsername: function (authConfig, username, password) {
    var fullCouchUrlWithUserPassword = makeUrlToRequestUserProfile(authConfig.couchUrl, username, password);
    return fetch(fullCouchUrlWithUserPassword)
      .then((res) => res.json())
      .then((json) => {
        if (json._id) usersCache[json._id] = {
          time: new Date().getTime(),
          user: json,
        };
        return json;
      })
    ;
  },

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
