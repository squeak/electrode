
var appName = {{ appName }};
var appVersion = {{ appVersion }};
var appMode = {{ appMode }};
var urlsToCache = {{ urlsToCache }};

var cacheName = "electrode-cache---"+ appName +"__"+ appVersion;

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  INSTALL SERVICE WORKER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(cacheName).then(function (cache) {
      if (appMode == "dev") return // do not add any cache in developpment mode
      else return cache.addAll(urlsToCache);
    })
  );
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  SERVE FILES
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

// try network and if fail, fall back on cache
// self.addEventListener("fetch", (event) => {
//   return event.respondWith(
//     fetch(event.request).catch(() => caches.match(event.request.url))
//   );
// });

// try to get file from cache, if it fails, get it from the network and cache it
// self.addEventListener("fetch", function (e) {
//   e.respondWith(
//     caches.match(e.request).then(function (cachedResponse) {
//
//       console.info("[ServiceWorker] Fetching resource: "+ e.request.url);
//
//       // return cached file
//       if (cachedResponse) return cachedResponse
//
//       // not in cache: fetch element
//       else fetch(e.request).then(function (response) {
//         return caches.open(cacheName).then(function (cache) {
//
//           console.info("[ServiceWorker] Caching new resource: "+ e.request.url);
//
//           // add fetched response to cache
//           cache.put(e.request, response.clone());
//           // return fetched response
//           return response;
//
//         });
//       });
//
//
//     })
//   );
// });

// try to get requests from cache, if fails, get it from network
self.addEventListener("fetch", function (e) {
  e.respondWith(
    caches.match(e.request).then(function (cachedResponse) {

      // return cached file
      if (cachedResponse) {
        console.info("[ServiceWorker] Fetched resource from cache: "+ e.request.url);
        return cachedResponse;
      }

      // not in cache: return fetched element
      else {
        return fetch(e.request).then(function (response) {
          console.info("[ServiceWorker] Fetched resource from server: "+ e.request.url);
          return response;
        }).catch(function (err) {

          // if no network
          console.info("[ServiceWorker] Cannot fetch resource from server: "+ e.request.url +" (you are probably offline)");

          // return json error
          if (!e.request.destination) return new Response(
            JSON.stringify({ ok: false, error: "offline", errorDetails: err, }),
            {
              headers: { "Content-Type": "application/json" },
              status: 400,
            }
          )

          // or error page if a page was asked // TODO: this could be improved, because maybe it's not an error page but another that should be returned
          else return caches.match("/error/");

        });
      };

    })
  );
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REMOVE CACHES FROM PREVIOUS VERSIONS
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

self.addEventListener("activate", function (e) {
  e.waitUntil(
    caches.keys().then(function (keyList) {

      return Promise.all(keyList.map(function (key) {
        if (key !== cacheName) {
          console.log("["+ cacheName +"] Removed old cached element: "+ key);
          return caches.delete(key);
        }
      }));

    })
  );
});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UPDATE SERVICE WORKER TO NEW VERSION NOW
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

self.addEventListener("message", function (event) {

  // udpate app with new service worker
  if (event.data.action === "updateNow") self.skipWaiting();

});

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
