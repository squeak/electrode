const _ = require("underscore");
const $$ = require("squeak/node");
const utilities = require("../utilities");
const $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DEFAULT URLS TO CACHE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

var defaultUrls = {

  // FAVICON
  favicon: [
    "/share/favicon.png",
  ],

  // ICOMOON FONT
  icomoon: [
    "/stylectrode/icomoon/fonts/icomoon.ttf?m50a5v", // this may change if stylectrode changes icomoon?
  ],

  // LATO FONT (BASIC IN STYLECTRODE)
  font_lato: [
    "/stylectrode/fonts/Lato/Lato-Black.ttf",
    "/stylectrode/fonts/Lato/Lato-Bold.ttf",
    "/stylectrode/fonts/Lato/Lato-Hairline.ttf",
    "/stylectrode/fonts/Lato/Lato-Light.ttf",
    "/stylectrode/fonts/Lato/Lato-Regular.ttf",
  ],

  // DEJAVUSANSMONO FONT (BASIC IN STYLECTRODE)
  font_dejaVuSansMono: [
    "/stylectrode/fonts/DejaVuSansMono/DejaVuSansMono.ttf",
  ],

  // ZILLASLAB FONT (ANOTHER EXAMPLE, NOT ACTIVATED BY DEFAULT, SEE defaultConfig.js)
  font_zillaSlab: [
    "/stylectrode/fonts/ZillaSlab/ZillaSlab-Bold.otf",
    "/stylectrode/fonts/ZillaSlab/ZillaSlab-Light.otf",
    "/stylectrode/fonts/ZillaSlab/ZillaSlab-Regular.otf",
    "/stylectrode/fonts/ZillaSlab/ZillaSlabHighlight-Bold.otf",
    "/stylectrode/fonts/ZillaSlab/ZillaSlabHighlight-Regular.otf",
  ],

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE LIST OF PAGES URLS TO CACHE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function getPagesUrlsAndScriptsAndStyles (pagesToCache, pagesList, appConfig) {

  var pagesRelatedUrlsToCache = [];

  _.each(pagesToCache, function (url) {
    pagesRelatedUrlsToCache.push(url);
    var page = _.findWhere(pagesList, { url: url, });
    if (!page) $$log.error("You specified a page caching url that doesn't correspond to any page listed in your routes: '"+ url +"'")
    else {
      pagesRelatedUrlsToCache.push(utilities.makePageDistPath(appConfig, page, "css"));
      pagesRelatedUrlsToCache.push(utilities.makePageDistPath(appConfig, page, "js"));
    };
  });

  return pagesRelatedUrlsToCache;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  MAKE LIST OF URLS TO CACHE
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: make the list of urls to cache for offline usage of the app
  ARGUMENTS: (
    !cachingPreferences <{
      ?defaults: <false|{
        favicon: <boolean>,
        icomoon: <boolean>,
        font_lato: <boolean>,
        font_zillaSlab: <boolean>,
      }> « if false, will not add any default urls to cache, otherwise will add all the ones set to true »,
      ?pages: <string[]>,
      ?customUrls: <string[]>,
      ?manifest: <false|object> « if not false or undefined, will add /pwa.webmanifest url to urls to cache »,
      ... any other key will be ignored
    }>,
    !pagesList <see router/route/pages:makePagesList·return>,
    ?appConfig,
  )
  RETURN: <string[]>
*/
module.exports = function (cachingPreferences, pagesList, appConfig) {

  return _.flatten([
    // default urls (if asked)
    cachingPreferences.defaults ? [
      cachingPreferences.defaults.favicon ? defaultUrls.favicon : [],
      cachingPreferences.defaults.icomoon ? defaultUrls.icomoon : [],
      cachingPreferences.defaults.font_lato ? defaultUrls.font_lato : [],
      cachingPreferences.defaults.font_zillaSlab ? defaultUrls.font_zillaSlab : [],
    ] : [],
    // pages urls (if asked)
    cachingPreferences.pages ? getPagesUrlsAndScriptsAndStyles(cachingPreferences.pages, pagesList, appConfig) : [],
    // custom urls (if some where passed)
    cachingPreferences.customUrls ? cachingPreferences.customUrls : [],
    // pwa manifest url
    cachingPreferences.manifest ? ["/pwa.webmanifest"] : [],
  ]);

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
