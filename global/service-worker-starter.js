var uify = require("uify");
var $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  REGISTER SERVICE WORKER
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function registerServiceWorker (electrode) {

  //
  //                              REGISTER SERVICE WORKER

  // if service worker was not served at domain root, to make it able to cache not only it's children urls, do the following:
  // navigator.serviceWorker.register("/somewhere/service-worker.js", { scope: "/", })...
  // also it should be specified in the header of all requests served that service worker is allowed as root (see boot/node/index.js)
  navigator.serviceWorker.register("/service-worker.js").then(function (registration) {

    // CANCEL REGISTRATION IF IN DEVELOPPMENT MODE (done here, because if done before registration if switched from production to developpment, the production worker still operates and should be unregistered)
    if (electrode.mode == "dev") {
      $$log.warning("Service worker has been disabled in developpment mode because it will necessarily fail.\nThe service worker would request scripts and styles before they have been generated.")
      registration.unregister();
      return;
    };

    // SAVE SERVICE WORKER REGISTRATION IN ELECTRODE OBJECT, SO THAT SOMEONE CAN DO SOMETHING WITH IT OUTSIDE OF ELECTRODE
    electrode.serviceWorkerRegistration = registration;

    // SUCCESS
    $$log.info("Successfully installed "+ electrode.appName +" version "+ electrode.appVersion); // \nScope is:", registration.scope

    // IF NEW VERSION OF SERVICE WORKER IS AVAILABLE
    registration.addEventListener("updatefound", function () {

      $$log.info("An update has been found for "+ electrode.appName +".");

      var newServiceWorker = registration.installing;
      newServiceWorker.addEventListener("statechange", function () {
        // TODO: this will enable the service worker right now, the user should reload which is proposed bellow, but since it's not mandatory, if they don't do it, there could be strange things happenning
        if (navigator.serviceWorker.controller && newServiceWorker.state == "installed") newServiceWorker.postMessage({ action: "updateNow" });
      });

    });

  }).catch(function(error) {
    $$log.error("Failed to register service worker: ", error);
  });

  //
  //                              RELOAD PAGE ON SERVICE WORKER REFRESH

  var proposedRefreshing;
  navigator.serviceWorker.addEventListener("controllerchange", function () {
    if (proposedRefreshing) return;
    proposedRefreshing = true;
    proposeToRefreshApp();
  });

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  DISPLAY TOAST THAT PROPOSES TO UPDATE THE APP
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

function proposeToRefreshApp () {

  uify.toast({
    dismissOnClick: false,
    duration: 0,
    class: "electrode_service_worker-update_toast",
    message: function ($container) {
      $container.div({ htmlSanitized: "A new version of this app<br>has been installed" });
      uify.button({
        $container: $container,
        title: "please refresh this page clicking here",
        inlineTitle: true,
        click: function () {
          window.location.reload();
        },
      });
    },
  });

};

// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
// //                                                  UPDATE SERVICE WORKER
// //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//
// var firstTime = true;
// function updateServiceWorker (electrode, registration) {
//
//   if (firstTime) registration.update().then(function (al) {
//
//     // REREGISTER WITH NEW CODE
//     $$log.info("Successfully updated service worker from server, now will reregister.");
//     firstTime = false;
//     registration.unregister();
//     registerServiceWorker(electrode);
//
//     // registration.active.postMessage(JSON.stringify({uid: "AAAAA", token: "BBBB"}));
//   }).catch(function (err) {
//     $$log.info("Failed to update service worker from server. If you are offline that's normal.");
//     $$log.error(err);
//   });
//
// };

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = function (electrode) {

  // ENABLE CACHING
  if (electrode.pwaCache) {
    if ("serviceWorker" in navigator) registerServiceWorker(electrode)
    else $$log.warning("Your browser doesn't support offline usage of "+ electrode.appName +" app.")
  };

  // ADD LINK TO PWA MANIFEST
  if (electrode.pwaManifest) $("head").link({ rel: "manifest", href: "/pwa.webmanifest", });

};
