
//
//                              LIBRARIES ACCESSIBLE IN ALL PAGES

window._ = require("underscore");
window.$$ = require("squeak");
require("squeak/extension/dom");
window.$ = require("yquerj");
window.$app = $("#electrode-app");

//
//                              START SERVICE WORKER (WILL ONLY DO IT IF ASKED IN CONFIG)

if (!page.noServiceWorker) require("./service-worker-starter")(electrode);

//                              ¬
//
