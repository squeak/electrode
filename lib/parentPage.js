require("squeak/extension/url")

if (document.location.pathname !== "/") {

  var $previousPageButton = $app.a({
    id: "electrode-previous_page",
    title: "go to parent page",
    href: $$.url.parent(page.url),
  });
  
  window.disablePreviousPageButton = function () {
    $previousPageButton.remove();
  }

};
