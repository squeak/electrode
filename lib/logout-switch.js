var electrodeLogout = require("./logout");

//
//                              SWITCH IN APP BODY

var $electrodeLogoutSwitchContainer;
function displayLogoutSwitch (toolbar) {

  $electrodeLogoutSwitchContainer = $app.a({
    class: "electrode-login-switch",
    title: "logout",
    href: "#",
  }).click(electrodeLogout);

  // user icon
  if (page.authenticatedUser.icon) $electrodeLogoutSwitchContainer.div({
    class: "icon icon-"+ page.authenticatedUser.icon,
  });

  // if no icon, user first letter of username
  else {
    var username = $$.getValue(page, "authenticatedUser.name") || $$.getValue(page, "authenticatedUser.username");
    $electrodeLogoutSwitchContainer.div({
      class: "text",
      text: _.isString(username) && username.length ? username[0].toUpperCase() : "X",
    });
  };

};

//
//                              SWITCH IN TOOLBAR

window.makeLogoutSwitchInToolbar = function (toolbar) {

  if ($electrodeLogoutSwitchContainer) $electrodeLogoutSwitchContainer.remove();

  toolbar.add.separator({
    position: "top",
    positionizeFirst: true,
  });

  toolbar.add.button({
    icomoon: page.authenticatedUser.icon,
    title: "logout",
    position: "top",
    positionizeFirst: true,
    key: "shift + q",
    click: electrodeLogout,
  });

};

//
//                              DISPLAY SWITCH IF SOME USER IS AUTHENTICATED

if (page.authenticatedUser) displayLogoutSwitch();

//                              ¬
//
