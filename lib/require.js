var _ = require("underscore");
var $$ = require("squeak");
var $ = require("yquerj");
var $$log = $$.logger("electrode");

var requireElec = $$.scopeFunction({

  //
  //                              MAIN

  /**
    DESCRIPTION: load scripts or styles browser side
    ARGUMENTS: (
      !scriptToLoad <string|string[]> « url to script(s) or style(s) »
      ?callback <function> « executed when script or style has been loaded »,
    )
    RETURN: <void>
  */
  main: function (scriptToLoad, callback) {

    // MULTIPLE SCRIPTS ASKED
    if (_.isArray(scriptToLoad)) {
      $$.asyncEach(scriptToLoad, function (d, i, asyncIterationIncrement) {
        requireElec(d, asyncIterationIncrement)
      }, callback);
    }

    // ONE SCRIPT ASKED
    else {
      // assume it's a library path and load script or style according to extension
      if (scriptToLoad.match(/\.js$/)) requireElec.load.script(scriptToLoad, callback)
      else if (scriptToLoad.match(/\.css$/)) requireElec.load.css(scriptToLoad, callback)
      else if (scriptToLoad.match(/\.less$/)) requireElec.load.less(scriptToLoad, callback) // should work but not verified
      // if failed to identify script log
      else $$log.error("what happened? seem that electrode.require failed, no script is loaded! : "+ scriptToLoad +" (probably url doesn't end by css or js and is not known lib, what is it?)");
    };
  },

  //
  //                              PAPER

  paper: function (scriptToLoad, canvaId, callback) {
    $('<script/>', {
      type: 'text/paperscript',
      src: scriptToLoad,
      canvas: canvaId,
    }).appendTo("#electrode-app").ready(callback);
  },

  //
  //                              LOAD

  load: {

    // load css style function
    css: function (url, callback) {
      $('<link/>', {
        rel: 'stylesheet',
        type: 'text/css',
        href: url,
      }).appendTo('head').ready(callback);
    },

    // // load less style function // is this still working? less can really be interpretted in browser?
    // less: function (url, callback) {
    //   $('<link/>', {
    //     rel: 'stylesheet/less',
    //     type: 'text/css',
    //     href: url,
    //   }).appendTo('head').ready(callback);
    // },

    // load script function
    script: function (url, callback) {
      $.ajax({
        url: url,
        async: false,
        cache: true,
        dataType: 'script',
        error: function (err) {
          $$log.error("THERE WAS AN ERROR LOADING : "+ url);
          document.write('<script type="text/javascript" src="'+ url +'"><\/script>'); // to know at which line there is an error
        },
        success: callback, //with callback as second function variable
      });
    },

  },

  //                              ¬
  //

});

module.exports = requireElec;
