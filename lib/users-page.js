var Userstilities = require("./users-utilities");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION: create users handling page
  ARGUMENTS: ({
    ?$container: <yquerjObject>@default=$app,
    ?additionalUserActions: <uify.button·options[]> «
      additional actions that can be done on this user,
      in buttons click actions, context is the user profile object,
    »,
    ?serverUrl: <string> «
      address to the couch server used for authenticating users in this app,
      if not defined, will be asked to the user when opening the page
    »,
    ?adminUsername: <string> «
      username of an administrator of this couch server,
      if not defined, will be asked to the user when opening the page,
    »,
    ?adminPassword: <string> «
      password for the chosen admin user,
      if not defined, will be asked to the user when opening the page
    »,
    ?background: <string> « an image to use as background »,
    ?backgroundCredits: <htmlString> « a credits for this background image, to write in small in bottom-left »,
  })
  RETURN: <void>
*/
module.exports = function (options) {

  //
  //                              OPTIONS

  var defaultOptions = {
    $container: $app,
    ready: function () {
      var userstilities = this;

      // CREATE LIST AND REFRESH IT ON CHANGES
      userstilities.refreshList();
      userstilities.pouch.changes().on("change", function () { userstilities.refreshList(); });

    },
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              BACKGROUND AND BACKGROUND CREDITS

  options.$container.addClass("electrode-users-page");
  if (options.background) options.$container.css({ backgroundImage: 'url("'+ options.background +'")', });
  if (options.backgroundCredits) options.$container.div({
    class: "background_credits",
    htmlSanitized: options.backgroundCredits,
  });

  //
  //                              INITIALIZE

  // CREATE userstilities
  var userstilities = new Userstilities (options);

  //                              ¬
  //

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
