var _ = require("underscore");
var $$ = require("squeak");
var uify = require("uify");
var asyncEach = require("async/each");
var $$log = $$.logger("electrode");

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//                                                  UNREGISTER SERVICE WORKER IF NECESSARY
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

/**
  DESCRIPTION:
    request to unregister from service worker if there is one,
    success will always fire, unless there is registrations and they fail to be unregistered
  ARGUMENTS: ( ø )
  RETURN: <Promise.then(ø).catch(<any>)
*/
function unregisterServiceWorkerIfNecessary () {

  return new Promise(function(resolve, reject) {

    // browser doens't support service worker
    if (!navigator.serviceWorker || !navigator.serviceWorker.getRegistrations) resolve()
    // get list of registrations
    else navigator.serviceWorker.getRegistrations().then(function (registrations) {

      // no registrations
      if (!registrations.length) resolve()
      // iterate registrations
      else asyncEach(registrations, function (registration, next) {

        registration.unregister().then(function (success) {

          // unregistering was successful
          if (success) next()
          // didn't unregister
          else next("[electrode] Unregister request returned false.");

        }).catch(reject);

      }, function (err) {
        if (err) reject(err)
        else resolve();
      });


    }).catch(resolve);

  });

};

//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||

/**
  DESCRIPTION:
    logout unregistering all service workers if there are some
    if it fails to unregister some service worker, it will display an error alert
  ARGUMENTS: ( ø )
  RETURN: <void>
*/
module.exports = function () {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  // unregister service worker (otherwise it remembers internal pages that should be forgotten (otherwise it's still possible to view them))
  unregisterServiceWorkerIfNecessary().then(function () {

    // request logout to server
    $$.open(window.electrodeLogoutUrl || "/logout/");

  }).catch(function () {

    // failed to unregister service worker and therfore logout
    uify.alert.error("Failed to log out.<br>See logs for more details.");
    $$log.error("Failed to log out.", err);

  });

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
