require("squeak/extension/url");

/**
  DESCRIPTION: display a fully working login page
  ARGUMENTS: (?options <{
    ?$container: <yquerjObject>@default=$app,
    ?loginUrl: <string>@default="/login/",
    ?imageUrl: <{
      ?default: <string>,
      ?unauthorized: <string>,
      ?autherror: <string>,
      ?mustlog: <string>,
      ?loggedOut: <string>,
      ?error: <string>,
    }> « images to show depending on the situation »,
    ?titleText: <{
      ?default: <string|function(url):<string>>,
      ?unauthorized: <string|function(url):<string>>,
      ?autherror: <string|function(url):<string>>,
      ?mustlog: <string|function(url):<string>>,
      ?loggedOut: <string|function(url):<string>>,
      ?error: <string|function(url):<string>>,
    }>,
    ?comment: <string|function($container)> « more text or any content to add below the input form »,
    ?background: <string> « an image to use as background »,
    ?backgroundCredits: <htmlString> « a credits for this background image, to write in small in bottom-left »,
    ?query: <{
      ?noRedirect: <boolean> « if this is true, will not redirect to asked url, but respond with a json object describing status of login request »,
      ?url: <string> « url to redirect to »;
    }> «
      if this login form is not intended to be in a login page, you can pass here the query details to create the form
      for example, you can specify that you just want a json response from the server to the login attempt, instead of being redirected to the asked page
      when this option is not defined, login-page will get those options from the page query
    »,
    ?noSubmitButton: <boolean> « set this to true if you want to make your own submit button »,
    ?ajaxSubmition: <boolean> « set this to true if you want to submit the login form as ajax request »,
    ?ajaxSuccessCallback: <function(response)> « callback on ajax submission success »,
    ?ajaxErrorCallback: <function(errorResponse)> « callback on ajax submission error »,
  }>)
  RETURN: <void>
*/
module.exports = function (options) {
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

  //
  //                              MAKE FULL OPTIONS

  var defaultOptions = {
    $container: $app,
    loginUrl: "/login/",
    imageUrl: {
      _recursiveOption: true,
      default: null,
      unauthorized: null,
      error: null,
      autherror: null,
      mustlog: null,
      loggedOut: null,
    },
  };
  options = $$.defaults(defaultOptions, options);

  //
  //                              GET QUERY

  // JUST MAKE A REQUEST TO LOGIN (THAT WILL NOT REDIRECT)
  if (options.query) {
    var query = options.query;
    var fullLoginUrl = $$.url.make({
      url: options.loginUrl,
      queryObject: options.query,
    });
  }
  // LOGIN AND REDIRECT TO ASKED PAGE
  else {
    var query = $$.url.query.getAsObject() || {};
    var fullLoginUrl = $$.url.make({
      url: options.loginUrl,
      queryObject: { url: query.url || "/", },
      hashString: $$.url.hash.getAsString(), // NOTE: this will also transmit hash to server, so that redirection can include hash
    });
  };

  //
  //                              BACKGROUND AND BACKGROUND CREDITS

  options.$container.addClass("electrode-login-page");
  if (options.background) options.$container.css({ backgroundImage: 'url("'+ options.background +'")', });
  if (options.backgroundCredits) options.$container.div({
    class: "background_credits",
    htmlSanitized: options.backgroundCredits,
  });

  //
  //                              CREATE FORM

  var $loginForm = options.$container.div({
    class: "login-form-container"
  }).formTag({
    class: "login-form",
    action: fullLoginUrl,
    method: "post",
  });

  //
  //                              CUSTOM SUBMITION

  if (options.ajaxSubmition) $loginForm.submit(function (event) {
    event.preventDefault();
    var formdata = $(this).serialize();
    $.ajax({
      type: "POST",
      url: $(this).attr("action"),
      data: formdata,
      success: options.ajaxSuccessCallback,
      error: options.ajaxErrorCallback,
    });
  });

  //
  //                              STATUS IMAGE

  var statusImageUrl = options.imageUrl[query.status] || options.imageUrl.default;
  if (statusImageUrl) $loginForm.img({
    class: "status_image",
    src: statusImageUrl,
  });

  //
  //                              TITLE

  var url = query.url || "this page";
  var titleText = {
    unauthorized: "You don't seem to be allowed to access "+ url,
    autherror: "Username or password is invalid, can't access to "+ url,
    error: "There's been an internal error logging in to access to "+ url +"<br>you should probably try to contact the administrator of this site to notify them of the problem",
    mustlog: "You must login to access to "+ url,
    default: "Login to access to "+ url,
    loggedOut: "<b>You have been successfully logged out!</b><br><br>If you want to login again:",
  };

  var titleTextForThisStatus = $$.getValue(options.titleText, query.status || "default") || titleText[query.status] || titleText.default;
  var $title = $loginForm.div({
    class: "title",
    htmlSanitized: $$.result(titleTextForThisStatus, url),
  });
  if (query.status == "unauthorized" || query.status == "autherror" || query.status == "error") $title.addClass("title-error");

  //
  //                              USERNAME

  var $usernameInputContainer = $loginForm.div({ class: "username", });
  $usernameInputContainer.label({ text: "username", });
  var $userNameInput = $usernameInputContainer.input({ name: "username", type: "text", });

  //
  //                              PASSWORD

  var $passwordInputContainer = $loginForm.div({ class: "password", });
  $passwordInputContainer.label({ text: "password" });
  $passwordInputContainer.input({ name: "password", type: "password", });

  //
  //                              SUBMIT

  if (!options.noSubmitButton) $loginForm.input({
    class: "submit",
    type: "submit",
    value: "log in",
  });

  //
  //                              COMMENT

  var $commentContainer = $loginForm.div({ class: "comment", });
  if (options.comment) {
    if (_.isFunction(options.comment)) options.comment($commentContainer)
    else $commentContainer.htmlSanitized(options.comment);
  };

  //
  //                              FOCUS USERNAME INPUT

  $$.dom.focus($userNameInput);

  //                              ¬
  //

  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
  //\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
};
