var uify = require("uify");
var loginPage = require("./login-page");
var $$log = $$.logger("electrode");

/**
  DESCRIPTION: create a login dialog
  ARGUMENTS: ({
    !loginCallback: <function(ø)> « executed on successfull login »,
    ?dialogTitle: <string> « title for the login dialog »,
    ?status: <string> « status returned by refused request »,
    ?imageUrl: <see login-page·options.imageUrl> « some images to show in the login dialog »,
    ?titleText: <see login-page·options.titleText> « custom titles text to display in the login dialog »,
    ?additionalButtons: <uify.button·options[]> « a list of custom buttons to add to login dialog »,
  })
  RETURN: <uify.dialog·return>
*/
function loginDialog (options) {

  var dialog = uify.dialog({
    title: options.dialogTitle,
    notClosable: true,
    buttons: options.additionalButtons ? _.flatten([ options.additionalButtons, "ok" ], true) : undefined,
    content: function ($container) {
      loginPage({
        $container: $container,
        titleText: options.titleText,
        imageUrl: options.imageUrl,
        query: { status: options.status, noRedirect: true, },
        noSubmitButton: true,
        ajaxSubmition: true,
        ajaxSuccessCallback: function (response) {
          if (!response.ok) $$log.error(response);
          options.loginCallback();
        },
        ajaxErrorCallback: function (error) {
          var errorResponse = error.responseJSON ? error.responseJSON : error || {};
          if (errorResponse.status) loginDialog($$.defaults(options, { status: errorResponse.status, }))
          else $$.alert.detailedError(
            "electrode/lib/login-dialog",
            "Unexpected error trying to log in.",
            errorResponse
          );
        },
      });
    },
    ok: function () {
      dialog.$content.find("form").submit();
    },
  });

  return dialog;

};

//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/
//\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/\/

module.exports = loginDialog;
