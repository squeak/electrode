# Start using electrode

*The harder option:* read [the documentation](?documentation) and build your app from scratch.  
*The easier option:* get [the template app](https://framagit.org/squeak/electrode-template), test it, and make your own app from it.
