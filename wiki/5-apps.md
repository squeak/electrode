# Apps using electrode

- [squik](https://squeak.eauchat.org): squeak's documentation website (this website ;))
- [dato](https://publicdato.eauchat.org/): a powerful app to create and handle databases with a customizable interface
- [chateau](https://chateau.eauchat.org/): a simple website

... if you want to add an app you made in this list, contact us at 'squeak [aATt] eauchat.org' ;)
