# Built-in utilities and libraries

Electrode provides a few built-in utilities and libraries to make navigation, authentication... in your app easier.
You may use them in any of your app's scripts requiring them, and importing their necessary stylesheets.


Here is the list of provided utilities:


## electrode/lib/parentPage

Display a triangular arrow in top-left corner to link to parent page.

**Usage:**
In your script:
```js
require("electrode/lib/parentPage")
```
In your stylesheet:
```js
@import "parentPage";
```

This will display parentPage arrow in the page and style it nicely. The `parentPage` library automatically create a `disablePreviousPageButton` function attached to the `window` object, so if needed, you can disable the parentPage button by simply calling: `disablePreviousPageButton()`.


## electrode/lib/login-page

Display a login box in the page (with nice images, if you set the `imageUrl` option).

**Usage:**
In your script:
```js
var loginPage = require("electrode/lib/login-page");
loginPage(loginPage_options)
// for example
loginPage({
  // list of paths to images to display in each situation
  imageUrl: {
    default:       "/pages/login/default.png",
    unauthorized:  "/pages/login/unauthorized.png",
    autherror:     "/pages/login/autherror.png",
    mustlog:       "/pages/login/mustlog.png",
  },
});
```
In your stylesheet:
```less
@import "login-page";
```

**All options:**
```js
loginPage_options = <{
  ?$container: <yquerjObject:default=$app>,
  ?loginUrl: <string:default="/login/">,
  ?imageUrl: <{
    ?default: <string>,
    ?unauthorized: <string>,
    ?autherror: <string>,
    ?mustlog: <string>,
  }> « images to show depending on the situation »,
  ?titleText: <{
    ?default: <string|function(url):<string>>,
    ?unauthorized: <string|function(url):<string>>,
    ?autherror: <string|function(url):<string>>,
    ?mustlog: <string|function(url):<string>>,
  }>,
  ?comment: <string|function($container)> « more text or any content to add below the input form »,
  ?background: <string> « an image to use as background »,
  ?backgroundCredits: <htmlString> « a credits for this background image, to write in small in bottom-left »,
  ?query: <{
    ?noRedirect: <boolean> « if this is true, will not redirect to asked url, but respond with a json object describing status of login request »,
    ?url: <string> « url to redirect to »;
  }> «
    if this login form is not intended to be in a login page, you can pass here the query details to create the form
    for example, you can specify that you just want a json response from the server to the login attempt, instead of being redirected to the asked page
    when this option is not defined, login-page will get those options from the page query
  »,
  ?noSubmitButton: <boolean> « set this to true if you want to make your own submit button »,
  ?ajaxSubmition: <boolean> « set this to true if you want to submit the login form as ajax request »,
  ?ajaxSuccessCallback: <function(response)> « callback on ajax submission success »,
  ?ajaxErrorCallback: <function(errorResponse)> « callback on ajax submission error »,
}>
```


## electrode/lib/login-dialog

Display a login dialog in your page, to let the user login easily without having to pass by a full login page.

**Usage:**
In your script:
```js
var loginDialog = require("electrode/lib/login-dialog");
loginDialog(loginDialog_options)
// for example
loginDialog({
  loginCallback: function () {
    alert("You have successfully logged in.");
  },
});
```
In your stylesheet:
```less
@import "login-dialog";
```

**All options:**
```js
loginDialog_options = <{
  !loginCallback: <function(ø)> « executed on successfull login »,
  ?dialogTitle: <string> « title for the login dialog »,
  ?status: <string> « status returned by refused request »,
  ?imageUrl: <see login-page·options.imageUrl> « some images to show in the login dialog »,
  ?titleText: <see login-page·options.titleText> « custom titles text to display in the login dialog »,
}>
```


## electrode/lib/logout-switch

Display a logout switch in bottom right corner when you're logged in.

**Usage:**
In your script:
```js
require("electrode/lib/logout-switch")
```
In your stylesheet:
```less
@import "logout-switch";
```

This will display the logout switch in bottom right corner when you're logged in.

If you use [toolbarify](https://squeak.eauchat.org/libs/toolbarify) and you would like the logout switch to be displayed on that bar, run this: `makeLogoutSwitchInToolbar(myToolbar)` where `myToolbar` is the object returned by toolbarify when you created the toolbar.


## electrode/lib/logout

Send a logout request to the server, making sure beforehand to unregister all service workers.

**Usage:**
In your script:
```js
var electrodeLogout = require("electrode/lib/logout")
```

You can then simply run `electrodeLogout()`, and this will log out the current user.
The `electrodeLogout` method doesn't receive any argument.


## electrode/lib/users-page

Display a list of the users of the couchdb instance used for authentication to this app. Let's you easily handle the list of users, add new ones, modify their roles, remove them.
⚠️ Don't enable authentication on the users page, unless you've already created some users, otherwise what users will you use to login to this page. Couch admin authentication is built in users-page, so it handles authentication for modification of the users list.

**Usage:**
In your script:
```js
var usersPage = require("electrode/lib/users-page");
usersPage(usersPage_options);
// for example
usersPage({
  serverUrl: "https://mycouch.tld/",
  background: "/assets/my-nice-background.jpg",
});
```
In your stylesheet:
```less
@import "users-page";
```

**All options:**
```js
usersPage_options = <{
  ?$container: <yquerjObject>@default=$app,
  ?additionalUserActions: <uify.button·options[]> «
    additional actions that can be done on this user,
    in buttons click actions, context is the user profile object,
  »,
  ?serverUrl: <string> «
    address to the couch server used for authenticating users in this app,
    if not defined, will be asked to the user when opening the page
  »,
  ?adminUsername: <string> «
    username of an administrator of this couch server,
    if not defined, will be asked to the user when opening the page,
  »,
  ?adminPassword: <string> «
    password for the chosen admin user,
    if not defined, will be asked to the user when opening the page
  »,
  ?background: <string> « an image to use as background »,
  ?backgroundCredits: <htmlString> « a credits for this background image, to write in small in bottom-left »,
}>
```


## electrode/lib/require

Require some script or styles from the browser (if for example they fail when required with browserify, with `require` or `@import` statements).

**Usage:**

```js
var requireElec = require("electrode/lib/require");
// import a script
requireElec("/path/to/script.js");
// or a stylesheet
requireElec("/path/to/style.css");
// if you want to import a paper script (see http://paperjs.org/) use the following:
requireElec.paper("/path/to/script.js");
// if you want, you can pass a second argument to requireElec, a callack function that will be executed once script or stylesheet is fully loaded
requireElec("/path/to/myScript.js", function () {
  // myScript is now loaded
});
```
