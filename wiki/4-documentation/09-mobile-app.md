# Make a mobile application

You can use electrode to easily make an app that can be installed in a mobile phone (thanks to the power of [Progressive Web Apps](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps) ;)).

To turn your code into an installable app, just add the following `pwa` key in your [config file](#config):

```
pwa: {

  // ENABLE OR DISABLE PWA CACHING
  cache: <boolean>@default=false « if this is true, caching using serviceWorker will be enabled, if you don't set this to true, all the following will be ignored »,

  // URLS TO CACHE
  // ⚠️ fill the following options with care, because if some urls fail to be reached, the installation of the serviceWorker will most probably fail entirely
  defaults: <false|{
    _recursiveOption: true,
    favicon: true,
    icomoon: true,
    font_lato: true,
    font_zillaSlab: false,
    font_dejaVuSansMono: true,
  }> « which electrode default resources should be cached »,
  pages: <string[]> « a list of url of pages of this app that should be cached (setting them here allows electrode to know it should cache their html, js and css) »,
  customUrls: <string[]> « a list of custom urls to cache »,

  // MANIFEST TO SETUP PWA INSTALLATION
  manifest: <false|{see https://developer.mozilla.org/en-US/docs/Web/Manifest to know what this can contain}> «
    you can safely ignore this if you want to use the default manifest of which values are extracted from your package.json file (see default configurations in settings/defaultConfig.js for details)
    however, it is recommended that you customize up at least "icons" to make sure an appropriate icon is used to install your app
  »,

}
```

That's it, you're done, now your app can be installed in a mobile phone (and in some desktop browsers as well) clicking the built-in browser [add to home screen button](https://developer.mozilla.org/en-US/docs/Web/Progressive_web_apps/Add_to_home_screen).

Now that your apps is usable offline, the way a browser will know that it should fetch a refreshed version of your app will be through it's version id. Simply change the version of your app in your package.json, restart your app server, and browsers will be notified on their next connection that there is a new version that should replace the previous one.

If you don't want serviceWorker to be loaded in all your pages. For example, you don't want it on a login page, but only after the user has logged in (otherwise it would fail to install properly), you can use the `noServiceWorker` option in your route page object.
