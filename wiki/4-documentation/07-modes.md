# Modes

Electrode proposes two different modes: *production* and *development*.
You can switch from one to the other changing the value of the `production` key in your `boot/config.js` file between true and false.

## `production: false`

In development mode, at every page request, the code of the page will be browserified on the fly and served.
This is especially convenient to quickly test modifications without having to restart the whole server.

## `production: true`

When production mode is enabled, all pages scripts and styles are browserified and minified at server start.
This makes serving pages much faster, but starting the server may take a few minutes (the time it takes to browserify and minify all the pages your app contains).
