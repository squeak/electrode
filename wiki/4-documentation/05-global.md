# Global scripts and styles

Some elements will be available in any electrode page. Some default libraries and stylings.
Here is a list of the present global elements that are imported in all pages, and how you can setup your own custom global elements for your app.

## electrode's global elements

All the following libraries and styles are included in any page served by electrode (this means they are attached to the window object):

### libraries

- [underscore](https://underscorejs.org/) (as `_`)
- [squeak](https://squeak.eauchat.org/libs/squeak) (as `$$`)
- [yquerj](https://squeak.eauchat.org/libs/yquerj) (as `$`)

### methods

- `$app`: this is just a shortcut for `$("#electrode-app")`, `#electrode-app` is the main container where it's recommended to put all your displayed contents
- `page`: this object contains the configuration of this page as it's passed in `routes/pages.js`, it also contains a list of children pages (recursive if they have public access or you're authentified and have access to the pages), for more details on the type of this object, [see here](./routes#pages)
- `page.authenticatedUser`: if a user is currently authenticated, the `window.page` object described previously will contain an additional key: `authenticatedUser`, containing details about the current user (the type of `page.authenticatedUser` is the following one: `<{ username: <string>, roles: <string[]>, icon: <string>, ... any other custom key that have been added to the user's profile }>`)


### styles

The following are the default styles included in all pages:
```less
// BODY GENERAL OPTIONS
body {
	font-family: "Lato-Light", Verdana, sans-serif;
	background-color: #333;
	color: #fff;
	margin: 0px;
	padding: 0px;
	font-size: 12px;
}

// so that children elements are proportionned to ALL
#electrode-app {
	top: 0;
	left: 0;
  position: relative;
  height: 100vh;
  width: 100vw;
  overflow-x: auto;
  overflow-y: auto;
}

// so that the padding is not added to global dimensions
* { -webkit-box-sizing: border-box; -moz-box-sizing: border-box; box-sizing: border-box; }
// LINKS GENERAL OPTIONS
a { cursor: pointer; }
a:link { color: white; text-decoration: none; }
a:visited { color: white; }

input {
	border-radius: 0;
	border: none;
	padding: 5px 10px;
}
```

In addition to this, [stylectrode's styles and default theme](https://squeak.eauchat.org/libs/stylectrode) are included in all pages.


## app's global elements

To add some elements in all your app's pages, you should add them in the `global/app.js` for code and `global/app.less` for styles.


## other global elements

The `global/` directory is also a good place to store scripts and styles that you may want to use in various pages. Name them as you want and import them in your page with for example: `require("../../global/some-script")` and `@import some-styles`.
