# Imports in less files

*How are `@import` statements resolved in `less` files?*
Since less is not as well designed as nodejs when it comes to importing/requiring packages to use them in a script.
It doesn't resolve all paths as cleverly, but to compensate, when an `@import` has to be resolved, it proposes to search for the given path in a group of directories that one can extend as desired.

Electrode is designed to make it as seamless as possible to import less files locally, and from external libraries.
So let's say `some-library` has a stylesheet included and you want to import it in your page, you should only need to write the following: `@import "some-library/path/to/stylesheet"` and it will be found.

To achieve this, the following paths are automatically searched to resolve `@import` statements:
 - the electrode global directory (="/path/to/your/app/node_modules/electrode/global/")
 - your app's global directory (="/path/to/your/app/global/")
 - your app's node_modules (="/path/to/your/app/node_modules/") <= this is where our library's stylesheet was found in the previous example
 - your app's public libraries (="/path/to/your/app/public/lib/")
 - electrode's libraries (="/path/to/your/app/node_modules/electrode/lib/")

## When importing local packages

If you use local libraries in your app that you import with statements like `"my-selfmade-lib": "file:../my-selfmade-lib"` in your package.json, you will have a problem. Because if that library is importing some stylesheets from a sublibrary, that sublibrary is located in "/path/to/your/app/node_modules/my-selfmade-lib/node_modules/sub-library/".
Yes it's getting a bit complicated, but it's simple also ;)
In such situation, less wouldn't resolve paths properly, therefore the `resolveSubmodulesPathsRecursivelyForLessImportStatments` option can be set in `boot/config.js` (I know that's a very long name for an option, but maybe better be specific sometimes no?). Setting this option to `always` or `once` will trigger the previous list of locations where to search in when resolving `@import` statements to be extended with all submodules' `node_modules` directories recursively.

In practice, this means that if for example you have two local libraries (let's say: `some-cats-provider` and `more-cats`) installed in your `node_modules`, it will add the following paths to the list of potential paths where to find less files to import:
 - "/path/to/your/app/node_modules/some-cats-provider/node_modules/"
 - "/path/to/your/app/node_modules/more-cats/node_modules/"
