# Routes

Electrode proposes to list pages routes and custom ones in different fashion.
Pages listed in `routes/pages.js` will be served getting the scripts and styles located in the `pages/` directory.
In parallel with this, you can specify any kind of custom route (GET/POST/PUT/DELETE) to achieve any kind of custom functionality (like retrieving some contents, posting files...).


## Pages

The `routes/pages.js` file should export a simple collection (an array of objects). Each entry of the collection is a `pageRouteObject` that describe a page to create route for.

For example it could look like this:
```js
module.exports = [
  {
    url: "/",
    title: "homepage",
  },
  {
    url: "/some/other/page/",
    title: "other page's title",
  },
]
```
This means two pages will be served in your app: "/" and "/some/other/page/".

The following describes what key/values should/can contain a `pageRouteObject`:
> 🛈 The following uses squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈

```javascript
pageRouteObject = {

  !url: <string> « absolute url »,

  ?redirect: <string> « if defined, instead of serving a page, this url will redirect to the given value »,

  ?name: <string> « if not defined will be autofilled by electrode with the last chunk of the pageRouteObject.url »,
  ?title: <string> « if not defined will be set to pageRouteObject.name's value »,
  ?path: <string> «
    path to script and styles folder (relative to the pages directory)
    normally you'd want to leave this to it's default value that will be automatically set from pageRouteObject.url (pageRouteObject.url.replace(/^\//, ""))
  »,
  ?customPagesFolderPath: <string> « if this page is in a custom directory (compared to other in the app), you can specify here the directory of the page (relative to your app root) »,
  ?data: <string> «
    path to pages data (assets)
    normally you'd want to leave this to it's default value (appConfig.browserPath.pagesData + pageRouteObject.path)
  »,

  ?auth: <{
    ?users: <string[]>,
    ?roles: <string[]>,
  }> «
    list of users and roles authorized to access this page,
    if auth is undefined, access is public
  »,

  ?noServiceWorker: <boolean>@default=false « if true and pwa is enabled in your config file, you can prevent a page from calling service worker using this option »,

  ?pageHeaders: <{ [header-name]: <header-value> }> «
    custom headers to set for this page
    (default page headers to use for all pages can also be set from the global config)
    any header that is not defined in this object will default to the global config header value
    by default the Content-Security-Header is set to only accept local scripts and styles, if you need a more laxist policy, customize it for the page or globally in your config file
  »,

  // in addition to these keys, an additional "children" key will be created with a list of this page's children pages, to be passed to the page object in the browser

}
```


## Redirections

If the `redirect` key is specified in a page object, the url will redirect to the passed value.


## Custom routes

Any file in `routes/custom` and it's subdirectories will be imported and their export will be merged to make a full list of custom routes.

Example file that would be located `routes/custom/test/`:
```js
module.exports = [

  {
    url: "/a/root/request/", // this request will be accessible at the following url: "/a/root/request/"
    type: "POST",
    func: function (req, res, next) {
      res.status(200).send({ message: "hello" });
    },
  },

  {
    // if the url is relative, (= it doesn't start with "/") the relative path of the file will be prepended to it
    url: "custom/request/", // this request will be accessible at the following url: "/test/custom/request/"
    type: "POST",
    func: function (req, res, next) {
      res.status(200).send({ message: "bye" });
    },
  },

]
```

All files in `routes/custom/` must return collections (arrays of objects) where each entry is a `customRouteObject`.

The following describes what key/values should/can contain a `customRouteObject`:
> 🛈 The following uses squyntax notation, information/documentation about it is available [here](https://squeak.eauchat.org/libs/squyntax/). 🛈

```javascript
customRouteObject = {

  !url: <string> «
    custom route's url
    if the url is relative, (= it doesn't start with "/") the relative path of the file will be prepended to it
    for example, in this file:
      "/custom/request/" will simply serve requests at "/custom/request/" and
      "/some/other/request/" will simply serve requests at "/some/other/request/" but
      "custom/request/" will serve requests at "/request/custom/request/" and another example:
      "some/other/req/" will serve requests at "/request/some/other/req/"
      all this because the file is named "./request" (relatively to the "routes/custom" directory)
  »,

  !type: <"GET"|"POST"|"PUT"|"DELETE"> « the REST type of request of this route (all REST types should be supported) »,

  !func: <function(req, res, next):<void>> «
    function to execute when a request has been made
    req and res are the default request and response objects generated by express routing method (for details see: https://expressjs.com/en/api.html#app)
  »,

  ?auth: <{
    ?users: <string[]>,
    ?roles: <string[]>,
  }> «
    list of users and roles authorized to access this page,
    if auth is undefined, access is public
  »,

}
```

By default, custom routes are sent with the "Cache-Control" header set to "no-cache, no-store, must-revalidate". If this is not an appropriate cache control for your needs, you can customize the header running something like `res.setHeader("Cache-Control", "must-revalidate");` before sending the response.
