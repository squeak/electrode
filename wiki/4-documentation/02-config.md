# The configuration file

The `boot/config.js` file contains the basic setup of your electrode app: it's name, at which port it will be served, the paths of the various directories that form the [architecture](#architecture) of your app.

Most values, can safely be ignored except the `appPath`.
Customize any setting according to your needs.


## The appPath setting

The `appPath` setting is used for electrode to determine where is the root of your app's file in your system, so it is recommended to use the default `path` module to resolve it.
Just import the `path` module: `let path = require("path");`
and then set `appPath: path.join(__dirname +"/../")` in your `config.js` file.

See the example configuration below if needed.


## The default configuration

The [default configuration](https://framagit.org/squeak/electrode/blob/master/settings/defaultConfig.js) file specifies the default values of each setting, what it does and what values you can use to customize them.


## An example of configuration

This is the config file used in the [electrode template app](https://framagit.org/squeak/electrode-template):
```js
let path = require("path");
module.exports = {
  name: "electrode-template-app",
  port: 9876,
  // don't change the following unless you move this script
  appPath: path.join(__dirname +"/../"),
  // set to true to precalculate all dist files on server start
  production: false,
  // calculate list of submodules to allow to easily find less style files in submodules <false|"always"|"once">
  resolveSubmodulesPathsRecursivelyForLessImportStatments: "once",
}
```
