# Architecture

**This is how an electrode app's structure look, with a [default config](#config).**
If you wish to have a different structure, you can create your own design and change the [config file](#config) to match the structure you want to use.

```bash
.
├── boot/
│   ├── config.js
│   └── index.js
├── dist/
│   ├── README
│   └── ... # dist files will automatically be put here to be served to page
├── global/
│   ├── app.js
│   ├── app.less
│   ├── README
│   └── ... # put your global scripts here
├── pages/
│   ├── index/
│   │   ├── index.js   # this is the code   that your index page ("/") will use
│   │   └── index.less # this is the styles that your index page ("/") will use
│   ├── test/
│   │   ├── test.js    # this is the code   that the page "/test/" will use
│   │   └── test.less  # this is the styles that the page "/test/" will use
│   │   └── childPage/
│   │   │   ├── childPage.js   # this is the code   that the page "/test/childPage/" will use
│   │   │   │── childPage.less # this is the styles that the page "/test/childPage/" will use
│   │   │   └── aScriptCalledByChildPage.js
│   │   └── ...
│   └── ... # add all your pages code here
├── public/
│   ├── lib/
│   │   ├── README
│   │   └── ... # libraries to load in the browser
│   ├── pages/
│   │   ├── index/
│   │   │   └── ... # whatever asset you need to use in index page ("/")
│   │   └── testPage/
│   │       │── thumbnail.png
│   │       │── ... # whatever asset you need to use in "/testPage/"
│   │       └── childPage/
│   │           └── ... # whatever asset you need to use in "/testPage/childPage/"
│   └── share/
│       │── favicon.png
│       └── ... # assets shared between pages
│── routes/
│   ├── custom/
│   │   │── request.js
│   │   └── ... # any file recursively will be processed to make a list of custom routes
│   └── pages.js # list all your pages urls in this file
├── package.json
└── README.md
```


## boot

The `boot` directory is the only one that is mandatory. This is where electrode will look to fetch your app's configuration and starting scripts.

It must contain the following files:

#### config.js

This file contains the configuration of your app.
See the [config wiki page](#config) for more information.

#### index.js

This file is used to start the nodejs express, server that will serve your code as a website on the port you chose in [config.js](#config).
It must contain the following code:

```javascript
let electrodeBooter = require("electrode/boot/node");
let config = require("./config.js");
electrodeBooter(config);
```


## dist

You should never need to look into `dist/`, electrode will put the generated scripts and styles of your app's pages in this directory, in order to serve them.


## global

You can put in the `global/` directory, the scripts and styles that you want to use in more than one page.
The `global/app.js` and `global/app.less` files are loaded in every page (before every page's own code). You can use them to apply global styles or scripts in your app.


## pages

The `pages/` folder should contain all your pages' codes and styles.

When you [register a page](#routes) in `routes/pages.js`, this will tell electrode to look for your page's code and styles in a directory called with the url you chose for your page.
For example:
  If you registered a page with the url: `/some-page/url/`,
  it's main script should be in the directory: `page/some-page/url/url.js`,
  and it's main stylesheet should be in: `page/some-page/url/url.less`.
The page `/otherpage/` scripts and styles will be in `page/otherpage/otherpage.js` and `page/otherpage/otherpage.less`.
The only exception to this is the root page `/`, it's code and styles should be in the `page/index/index.js` and `page/index/index.less` files.

Since pages don't have any html as an entry point, everything that a page shows is created by it's main script, and any of it's styles are in it's main stylesheet.
Since pages scripts and styles are processed by [browserify](https://browserify.org/) and [lesscss](https://lesscss.org/) before being sent to the browser, you can easily fragment your code, import libraries... with the basic nodejs' `require` syntax and less' `@import` statements.


## public

All static assets in the `public/` folder are served publicly at root path.
So for example, an image that is located at this path: `public/share/cat-pictures/rufus.jpg` will be accessible at the url `domain.tld/share/cat-pictures/rufus.jpg`.

By convention, assets specific to some pages are organized in the same hierarchical way that pages are in the `pages/` folder. This makes it easier to get pages' assets from a page's code using [the page object](#global).
So for example, the assets for page: `/some/deep/cool-page/` would be stored in `public/pages/some/deep/cool-page/`.

Assets used in many pages can be stored in `public/share/`.

Libraries that do not support server side calling and must be imported from the browser can be stored in `public/lib/` and then easily be imported with the [browser require utility](#utilities).

You can store any content and organize it however is good for your app in the `public/` folder. It will then simply be served on request.


## routes

The `routes/` directory should contain a `page.js` file and a `custom/` directory.
The `page.js` contains the [list of pages](#routes), [redirections](#routes) to serve ("GET" requests).
Every file in the `custom/` directory and it's subdirectories, will be imported and used to make a list of [custom routes](#routes) to serve ("GET", "POST", "PUT", "DELETE" requests).


## other files

#### package.json

Just your usual `package.json` file.
Note that it should at least have the following fields:
```json
{
  "main": "boot/index.js",
  "scripts": {
    "start": "node ./boot/index.js",
  },
  "dependencies": {
    "electrode": "git+https://git@framagit.org/squeak/electrode.git"
  }
}
```
This allow to simply start the nodejs server with `npm start`.

Also it is recommended that you fill the following fields as well:
```json
{
  "repository": {
    "type": "git",
    "url": "git://git.domain.tld/me/my-app.git"
  },
  "license": "AGPL-3.0-or-later"
}
```
This will allow electrode to display in each page, the license under which the code of your app is released, and the repository where it's source code can be found.

#### README.md

Your app's readme.
