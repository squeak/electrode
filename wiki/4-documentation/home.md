# Documentation

These pages document how to use electrode and all the possibilities it gives.

## Use electrode

To create an electrode app, you need to:
  - create a new nodejs package with [the proper architecture](#architecture),
  - add electrode with: `npm install --save git+https://git@framagit.org/squeak/electrode.git`,
  - then start the server with `npm start`,
  - visit your browser at `http://localhost:9876` or whichever port you chose in your config file.


## Docs

- [architecture of an electrode app](#architecture)
- the [config file](#config)
- handling [routes and pages](#routes)
- how to have some pages require [authentication](#authentication)
- [global scripts and styles](#global) applied to all pages
- some [utilities](#utilities) that electrode provide
- development and production [modes](#modes)
- how are [less `@import`](#styles) paths resolved
- make your site [installable to mobiles](#mobile-app)


## Issues, questions, remarks, contributions

If you have some issues, requests, remarks, feel very welcome to [file an issue](https://framagit.org/squeak/electrode/issues) or propose modifications to electrode :-)
