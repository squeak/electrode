# Authentication

Electrode provides optional built-in authentication.
This is how to set it up:


## Setup the url to users database

For it to work, you need a couchdb database, and to specify the address of this couchdb instance in your `boot/config.js` file.
This is how the config can look like:
```javascript
{
  authentication: {
    couchUrl: "http://couchdb.domain.tld/",
    loginUrl: "/login/",
    logoutUrl: "/logout/",
  },
}
```
By default, the `loginUrl` and `logoutUrl` are the url used to post login and logout request. You don't need to specify them unless you want to use custom ones.

In the `couchUrl` field, you should specify an address to a working couchdb database. Authentication is designed to work against the default `_users` database which list this couch instance's users.  

In other words, to add users, you must add them in that database instance. If you want to give them access to a private page in your app, add them (or a role they have) to the list of allowed users (in this [page's object](#routes), see below for details).


## Setup users (or roles) granted access to some specific pages in pages object

Once you've setup the url and couch instance listing users, you need to choose which users or roles (roles are just groups of users) can have access to which pages or custom routes.
In your page or custom route definition object, define auth like this:

```javascript
  auth: {
    users: ["someUserAuthorized", "someOtherUserAuthorizedToAccessThisPage"],
    roles: ["someRoleAuthorized", "someOtherRoleAuthorizedToAccessThisPage"],
  }
```

If a page object doesn't contain an auth object with either users or roles (or both), the page is considered public.

## Requests

Once config and routes have been properly setup, when visiting a page, if authentication has been required for it, you'll be redirected to a `/login/` page (or any custom route you decided to use instead).

You will need to create this page in the `pages/` folder.
You can use `electrode/lib/login-page` [utility](#utilities) in your login page and `electrode/lib/logout-switch` [utility](#utilities), to generate default login page and logout switch on the desired pages; `require` those libraries and `@import` their stylesheet in the pages you want to use them in.
If you chose to use custom `loginUrl` and/or `logoutUrl`, before requiring the libs in your page, define the urls like this: `window.electrodeLoginUrl = "/myCustom/login/url/"`, `window.electrodeLogoutUrl = "/myCustom/logout/url/"`.

You can also make your own login systems. This basically means making a form with a `username` and `password` fields, and which `action` url is `"/login/?url=/page-to-redirect-to/on-login-success/"`.
You can also customize the query adding the `noRedirect` option. This will prevent electrode from redirecting the user to any page, but will send a json response to say if the login succeeded or not. The response will be in the following format: `{ ok: <boolean>, status: <string> }`. For example: `{ "ok": false, "status": "mustlog" }`.

## Users

To handle users in the couch server you use for authentication, electrode provides the `electrode/lib/users-page` [utility](#utilities), that make it very easy to create a page listing users, add some, remove them...
