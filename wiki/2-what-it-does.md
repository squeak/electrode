# What electrode does for you

- serve your website at [the port you chose](?documentation#config)
- [make a mobile app](?documentation#mobile-app)
- propose a simple and customizable [architecture](?documentation#architecture) for your app
- built in [authentication](?documentation#authentication) for the pages that need it
- [pages list — customize favicon, titles...](?documentation#routes)
- [pages redirection](?documentation#routes)
- [custom routes](?documentation#routes) to answer to any kind of REST request
- make some [data publicly available](?documentation#architecture)
- [builtin utilities](?documentation#utilities) to
  - display a login page
  - display a logout button
  - display a button to jump to parent page
  - require some script or styles from the browser (if for example they cannot be required with browserify)
- some default [global scripts and styles](?documentation#global) that will be added in all your pages
- [two modes](?documentation#modes):
  - development: where pages scripts are rebuilt at every call, for easy modifications and testing
  - production: where pages and scripts are minified at server start and then served at every call, for even faster and lighter answers
