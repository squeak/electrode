

Electrode is a simple and light framework to create powerful webapps, that can also be installed in desktop and smartphones.

You can use it to create in a few minutes a website, that can also be installed in a mobile or computer (thanks to the power of Progressive Web App).

I you don't feel like reading this documentation you can just [start from an electrode template app](https://framagit.org/squeak/electrode-template) and modify it to make your own app from it.
