# Why electrode

The main motivation behind designing electrode, was a need for lightness and deeper control on what a website/application is doing.

## Lightness
Like vue.js, angular or react, electrode is some kind of framework.  
It is much less elaborate than those other big frameworks, but it's also easier to know what it really does, and it is lighter. It's goal is to propose a set of basic features that make it possible to have a website or program running in just a few minutes.

## Flexible architecture
Electrode proposes a prebuilt architecture for your app, but you can customize it for your own needs editing a simple config file.

## Programming oriented
Electrode proposes a design approach that has kind of gotten rid of html. Everything is achieved through scripting and styling. There is no preconception of content or templates, in order to open design possibilities, and propose a space for creating very dynamic and artistic designs.  
Furthermore, it offers you a clean and peaceful working environment, without fuss and mess ;)
